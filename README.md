This source code contains one solution of the stochastic unit commitment (SUC) problem.
It is divided into two packages.

The uc_sto package contains the model for the SUC problem.

Dependencies:
Pyomo - http://www.pyomo.org/installation/
Solver - We recommend Gurobi. (http://www.gurobi.com/downloads/download-center)

After the code is cloned and the solver installed and configured, to run the model use:
runph --default-rho=1000 --model-directory=models --instance-directory=nodedata --solver=gurobi

The sce_gen package contains the scenario generation artefacts.

Dependencies: 
pymc 
matplotlib
numpy
scipy

After running the scenarioGenerator.py script, the scenarios will be in the scenarios folder and should be moved to the uc_sto/nodedata folder.