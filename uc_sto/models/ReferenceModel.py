#
# Imports
#

from pyomo.core import *

#
# Model
#

model = AbstractModel()

#
# Parameters
#

# generators set
model.generator = Set(ordered=True)

# generator related parameters
model.minPower = Param(model.generator, within=NonNegativeReals)

model.maxPower = Param(model.generator, within=NonNegativeReals)

model.rampUpMin = Param(model.generator, within=NonNegativeReals)

model.rampUpMax = Param(model.generator, within=NonNegativeReals)

model.fuelCost = Param(model.generator, within=NonNegativeReals)

model.startUpCost = Param(model.generator, within=NonNegativeReals)

# simulation time
model.timePoints = Param(within=NonNegativeIntegers)

model.time = RangeSet(1, model.timePoints)

# demand at each simulation time
model.demand = Param(model.time, within=NonNegativeReals)

# renewable energy available at each time
model.renewablePower = Param(RangeSet(1, 96), within=NonNegativeReals)

#
# Variables
#

# power output for each generator at each simulation time
model.powerOutput = Var(model.time, model.generator, initialize=0, domain=NonNegativeIntegers)

# generator status: 0 - off; 1 - on
model.generatorStatus = Var(model.time, model.generator, within=Boolean, initialize=False)

# nominal production costs for all generators
model.productionCost = Var(within=NonNegativeReals)

# start up costs for all generators
model.fixedCost = Var(within=NonNegativeReals)

#
# Constraints
#

# generator power output more or equal than minPower
def PowerMinLimitRule(model, i, g) :
     return model.minPower[g] * model.generatorStatus[i,g] <= model.powerOutput[i,g]

model.powerMinLimitRequirement = Constraint(model.time, model.generator, rule=PowerMinLimitRule)

# generator power output less or equal than maxPower
def PowerMaxLimitRule(model, i, g) :
    return model.powerOutput[i,g] <= model.maxPower[g] * model.generatorStatus[i,g]

model.powerMaxLimitRequirement = Constraint(model.time, model.generator, rule=PowerMaxLimitRule)

# minimum ramp up constraint
def MinRampUpRule(model, t, g) :
    if t == 1 :
        return Constraint.Skip
    else :
        if model.generatorStatus[t, g] - model.generatorStatus[t - 1, g] != 1 and model.powerOutput[t, g] != model.powerOutput[t - 1, g]:
            return model.powerOutput[t, g] >= model.rampUpMin[g] + model.powerOutput[t - 1, g]
        else :
            return Constraint.Feasible

model.min_ramp_up_constraint = Constraint(model.time, model.generator, rule=MinRampUpRule)

# maximum ramp up constraint
def MaxRampUpRule(model, t, g) :
    if t == 1 :
        return Constraint.Skip
    else :
        if model.generatorStatus[t, g] - model.generatorStatus[t - 1, g] != 1 :
            return model.powerOutput[t, g] <= model.rampUpMax[g] + model.powerOutput[t - 1, g]
        else :
            return Constraint.Feasible

model.max_ramp_up_constraint = Constraint(model.time, model.generator, rule=MaxRampUpRule)

# generated power output equal demand minus available renewable
def DemandRule(model, i) :
    return sum(model.powerOutput[i, g] for g in model.generator) >= model.demand[i] - model.renewablePower[i]

model.demandConstraint = Constraint(model.time, rule=DemandRule)

# production cost
def production_cost_function(model):
    return model.productionCost == sum(value(model.fuelCost[g]) * model.powerOutput[i, g]
                                       for i in model.time for g in model.generator)

model.ComputeProductionCosts = Constraint(rule=production_cost_function)

# compute the total startup cost, across all generators and time periods.
def compute_fixed_cost_rule(model):
    total = 0
    for g in model.generator :
        for t in model.time :
            if t == 1 :
                total += model.startUpCost[g] * model.generatorStatus[t, g]
            else :
                total += model.startUpCost[g] * (model.generatorStatus[t, g] - model.generatorStatus[t - 1, g])
    return model.fixedCost == total

model.ComputeTotalFixedCost = Constraint(rule=compute_fixed_cost_rule)

def total_cost_rule(model) :
    return model.fixedCost + model.productionCost

model.Total_Cost_Objective = Objective(rule=total_cost_rule, sense=minimize)