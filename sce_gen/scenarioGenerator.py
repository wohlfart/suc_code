#importing the modules for the needed functionalities 
import pymc
import csv 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

# number of scenarios to generate
scenariosNum = 100

# number of data points per scenario
pointsNum = 96

# files with wind generated power
file_names = [
    'mwindeinsp_ist_prognose_2015_01.csv', 'mwindeinsp_ist_prognose_2015_02.csv', 'mwindeinsp_ist_prognose_2015_03.csv',
    'mwindeinsp_ist_prognose_2015_04.csv', 'mwindeinsp_ist_prognose_2015_05.csv', 'mwindeinsp_ist_prognose_2015_06.csv',
    'mwindeinsp_ist_prognose_2015_07.csv', 'mwindeinsp_ist_prognose_2015_08.csv', 'mwindeinsp_ist_prognose_2015_09.csv', 
    'mwindeinsp_ist_prognose_2015_10.csv', 'mwindeinsp_ist_prognose_2015_11.csv', 'mwindeinsp_ist_prognose_2015_12.csv', 
  	'mwindeinsp_ist_prognose_2016_01.csv', 'mwindeinsp_ist_prognose_2016_02.csv', 'mwindeinsp_ist_prognose_2016_03.csv'
    ]

n_months = len(file_names)
	
array = []

input_data = [[1 for x in range(n_months * 30 + 10 )] for x in range(96)]
gen_data = [[1 for x in range(96)] for x in range(scenariosNum)]

print("starting read the data")

day_count = 0
read_count = 0
        
# read all files, if the generated power is zero it is substituted by 1, because of MCMC algorithm restriction
for file in file_names :
    with open('data/' + file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        		
        for row in reader:
            value = row[5] # fifth column is the observed power
            if value.isdigit() :
                value = int(value)
                if value == 0 :
                    value = 1				
                
                input_data[read_count][day_count] = value				
                array.append(value)
  			
                read_count += 1
                if read_count == 96:
                    read_count = 0				
                    day_count += 1
					
plt.plot(array)
plt.title("Historical data")
plt.show()

plt.hist(array)
plt.title("Historical data histogram")
plt.show()

max = max(array)

# generate MCMC estimation for each time point
for i in range(0, pointsNum) :
    
	# declare parameters alpha and beta for Weibull distribution. 
    a = pymc.Uniform('a', lower=0, upper=max, value=1 , doc='Weibull alpha parameter')
    b = pymc.Uniform('b', lower=0, upper=max, value=142, doc='Weibull beta parameter')	   
	
	# historical data
    observed = pymc.Weibull('observed', alpha=a, beta=b, value=input_data[i][:], observed=True)
    # new generated data
    generated = pymc.Weibull('generated', alpha=a, beta=b)

	# creates the MCMC model
    M = pymc.MCMC([a, b, observed, generated])
	
	# set a and b values to their posterior
    pymc.MAP(M).fit()
	
    print("i = " + str(i) + " a = " + str(a.value) + " - b = " + str(b.value))
    print("-----------------")

	# sample the model
    M.sample(iter=20000, burn=18000, thin=1)
 	
	# save scenariosNum points to gen_data array discarding the values that are bigger than max
    count = 0	
    index = 0
    while count < scenariosNum :
        value = M.trace('generated')[:][index]
        if value <= max :
            gen_data[count][i] = value
            count += 1
        index += 1 			

plt.ylabel("Power (MW)")
plt.xlabel("Time")

# plot first scenario 
plt.plot(gen_data[0][:])
plt.title("First scenario")
plt.show()

# save scenarios files	
for i in range(0, scenariosNum) :
    scenarioFile = open("scenarios/scenario" + str(i + 1) + ".dat", "w")
    scenarioFile.write("#generated scenario number: " + str(i) + "\n\n")
    scenarioFile.write("param renewablePower := ")
    	
    total = 0
    for j in range(1, pointsNum + 1) :
        scenarioFile.write(" " + str(j) +  " " + str(gen_data[i][j - 1]))
        total += gen_data[i][j - 1]
    scenarioFile.write(" ;")
    scenarioFile.write("\n\n# total: " + str(total) + " avg: " + str(total/pointsNum))
    scenarioFile.close()
